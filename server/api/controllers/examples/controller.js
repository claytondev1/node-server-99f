import DatabaseService from '../../services/database.service';

export class Controller {
  all(req, res) {

    var options = {};

    options['host'] = 'tcp://0.tcp.ngrok.io';
    options['port'] = '19307';
    // options['database'] = '/Users/clayton/empresas/ALTERDB-EMPORIO.ib';
    options['database'] = 'D:\\FCERTA\\DB\\ALTERDB.ib';
    options['user'] = process.env.FIREBIRD_USER;
    options['password'] = process.env.FIREBIRD_PASSWORD;
    options['lowercase_keys'] = false; // set to true to lowercase keys
    options['pageSize'] = 4096;

    DatabaseService.all(options).then(r => res.json(r));
  }

  byId(req, res) {
    DatabaseService.byId(req.params.id).then(r => {
      if (r) res.json(r);
      else res.status(404).end();
    });
  }

  create(req, res) {
    DatabaseService.create(req.body.name).then(r =>
      res
        .status(201)
        .location(`/api/v1/examples/${r.id}`)
        .json(r)
    );
  }
}
export default new Controller();
