import l from '../../common/logger';
var Firebird = require('node-firebird');

class DatabaseService {
  constructor() {}

  all(options) {

    l.info(`${this.constructor.name}.all()`);

    var products = [];
    var _poll = this.pool; // Get a free pool
    
    return new Promise(function(resolve, reject) {
    
      Firebird.attach(options, function(err, db) {
    
        if (err)
          reject(err); 
  
        // db = DATABASE
        db.query('SELECT FIRST 100 * FROM FC03000', function(err, result) {
          
          for (let index in result) {
            products.push({
              cd_produto: result[index].CDPRO,
              nome: String(result[index].DESCR),
              desc_produto: String(result[index].DESCRPRD),
              dosagem_max: result[index].DOMAX,
              dosagem_max_unidade: String(result[index].UNIDM),
              dosagem_max_diaria: result[index].DOMAXD,
              dosagem_max_diaria_unidade: String(result[index].UNIDMD),
              uso_continuo: String(result[index].INDUSOCONT),
              cd_zanini: String(result[index].Z_CDFARM),
              cd_fiscal: String(result[index].CLFISC),
              sngpc: String(result[index].INDSNGPC),
              portaria_anvisa: String(result[index].PORTA),
              tipo_pesagem: String(result[index].TPPESA),
              preco_compra: (result[index].PRCOM*10000),
              preco_venda:  (result[index].PRVEN*10000),
              diluicao: result[index].DILUICAO,
              teor: result[index].TEOR,
              densidade: result[index].DENSIDADE,
              unidade_de_compra: String(result[index].UNIDA),
              status: String(result[index].SITUA),
              grupo:  String(result[index].GRUPO),
              deletado: String(result[index].INDDEL),
              liberado_para_venda: String(result[index].INDBLOQUEIO)
            });
          }
  
          // IMPORTANT: release the pool connection
          db.detach();
          resolve(products);
        }); 
      });
      
    });

  }

  byId(id) {
    l.info(`${this.constructor.name}.byId(${id})`);
    return Promise.resolve(this._data[id]);
  }

  insert(name) {
    const record = {
      id: this._counter,
      name,
    };

    this._counter += 1;
    this._data.push(record);

    return Promise.resolve(record);
  }
}

export default new DatabaseService();
